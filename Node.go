package goerl

//Node 并发节点
type Node struct {
	id           int64
	registerName string
	mailBox      *MailBox
	ExitFunc     func()
	UpdateFunc   func(int)
	UpdateSpeed  int64
}

//Start 启动Node
func (node *Node) Start() {
	go node.loop()
}

func (node *Node) loop() {
	node.mailBox.loop()
}

//ID 并发节点Node的唯一标识ID
func (node *Node) ID() int64 {
	return node.id
}

//RegisterName 注册的名字
func (node *Node) RegisterName() string {
	return node.registerName
}

//Close 关闭并发节点
func (node *Node) Close() {
	Send(node, []interface{}{":Exit", ":Normal"})
	if node.ExitFunc != nil {
		node.ExitFunc()
	}
}

//NewNode 创建一个新的并发节点
func NewNode(receiveFunc func(interface{})) *Node {
	retNode := &Node{
		mailBox:     NewMailBox(10, receiveFunc),
		UpdateSpeed: 1,
	}
	retNode.mailBox.node = retNode
	return retNode
}
